#include "header.h"

/* FUNCIONES GENERALES */

/* Agregar Cliente */
void agregar_cliente(estruc_cliente *cl) {
  int i;
  for (i = 0; i < CANT_CLIENTES; i++) {
    if (!clientes[i]) {
      clientes[i] = cl;
      return;
    }
  }
}

/* A través del "nickname" obtenemos el ID del cliente */
int buscar_cliente_por_nombre(char *nom) {
  for (int i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      if (strcmp(clientes[i]->nombre, nom) == 0) {
        return clientes[i]->cid;
      }
    }
  }
  return -1;
}

/* A través del "ID" obtenemos el nickname del cliente */
char *buscar_cliente_por_id(int cid) {
  for (int i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      if (cid == clientes[i]->cid) {
        return clientes[i]->nombre;
      }
    }
  }
  return NULL;
}

/* A través del "nickname" obtenemos el estado del cliente */
int buscar_saber_por_nombre(char *nom) {
  for (int i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      if (strcmp(clientes[i]->nombre, nom) == 0) {
        return clientes[i]->saber;
      }
    }
  }
  return -1;
}

/* Eliminar cliente */
void eliminar_cliente(int cid) {
  int i;
  for (i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      if (clientes[i]->cid == cid) {
        clientes[i] = NULL;
        return;
      }
    }
  }
}

/* Enviar mensaje a todos los clientes excepto al que esta enviando */
void enviar_mensaje(char *s, int cid) {
  int i;
  for (i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      if (clientes[i]->cid != cid) {
        if (write(clientes[i]->conex, s, strlen(s)) < 0) {
          perror("write");
          exit(-1);
        }
      }
    }
  }
}

/* Mensaje individual al cliente que esta conectado */
void mensaje_individual(const char *s, int conex) {
  if (write(conex, s, strlen(s)) < 0) {
    perror("write");
    exit(-1);
  }
}

/* Enviar mensaje a todos los clientes */
void mensaje_general(char *s) {
  int i;
  for (i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      if (write(clientes[i]->conex, s, strlen(s)) < 0) {
        perror("write");
        exit(-1);
      }
    }
  }
}

/* Enviar mensaje al cliente */
void mensaje_cliente(char *s, char *nombre) {
  int i;
  for (i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      if (strcmp(clientes[i]->nombre, nombre) == 0) {
        if (write(clientes[i]->conex, s, strlen(s)) < 0) {
          perror("write");
          exit(-1);
        }
      }
    }
  }
}

/* Enviar lista de clientes conectados */
void lista_clientes(int conex) {
  int i;
  char s[64];
  for (i = 0; i < CANT_CLIENTES; i++) {
    if (clientes[i]) {
      sprintf(s, "<<CLIENTE %d | %s\r\n", clientes[i]->cid,
        clientes[i]->nombre);
      mensaje_individual(s, conex);
    }
  }
}

void salto_linea(char *s) {
  while (*s != '\0') {
    if (*s == '\r' || *s == '\n') {
      *s = '\0';
    }
    s++;
  }
}

/* Imprimir dirección ip */
void direccion_ip(struct sockaddr_in direccion) {
  printf("%d.%d.%d.%d", direccion.sin_addr.s_addr & 0xFF,
   (direccion.sin_addr.s_addr & 0xFF00) >> 8,
   (direccion.sin_addr.s_addr & 0xFF0000) >> 16,
   (direccion.sin_addr.s_addr & 0xFF000000) >> 24);
}


/* FUNCIONES PARA LA BASE DE DATOS */

/* Insertar un mensaje a la BD en la conversación actual */
void insertar_msg(MYSQL *conn, int conversacion, char *hora, char *msg){
   
  char consulta[512];
  sprintf(consulta, "INSERT INTO `mensajes` (`id`, `conversacion_id`, `hora`, `msg`) VALUES (NULL, '%d', '%s', '%s');", conversacion, hora, msg);
   
  if((mysql_query(conn, consulta)==0)){
      printf("Se agrego un mensaje a la conversacion: %d \n", conversacion);

  }else{
      printf("Error no se pudo agregar el mensaje. %s \n", mysql_error(conn));
  }
}

/* Insertar una nueva conversación a la BD */
void insertar_conversacion(MYSQL *conn, char *fecha, char *user1, char *user2){
  
  char consulta[512];
  sprintf(consulta, "INSERT INTO `conversaciones` (`id_conversacion`, `fecha`, `user1`, `user2`) VALUES (NULL, '%s', '%s', '%s');", fecha, user1, user2);
   
  if((mysql_query(conn, consulta)==0)){
    printf("Se agrego una conversacion a la BD. \n");
  }else{
    printf("Error no se pudo crear la conversacion. %s \n", mysql_error(conn));
  }
}

/* Obtener el ID de la última conversación */
int buscar_id_conversacion(MYSQL *conn, MYSQL_ROW row, MYSQL_RES *res, char *consulta){
  //concatena las constantes a la variable consulta 
  sprintf(consulta, "SELECT  * FROM `conversaciones` ORDER BY `id_conversacion` DESC LIMIT 1");

  //hacemos consulta y verificamos que se cumple
  if((mysql_query(conn, consulta)==0)){

  //guardamos resultado en la variable resultado que es de tipo MYSQL_RES *
  res=mysql_store_result(conn);

  //leemos los datos almacenados en resultado y lo devolvemos a la variable row que es de tipo MYSQL_ROW
  while(row=mysql_fetch_row(res)){                                    
      return atoi(row[0]);
    }
  }
  mysql_free_result(res);
  //preguntamos si se ha llegado al final de nuestra variable resultado
  if(!mysql_eof(res)){
    printf("Error de lectura %s\n", mysql_error(conn));
  }
}

/* Imprime en pantalla la conversación especificada */
void buscar_conversacion(MYSQL *conn, MYSQL_ROW row, MYSQL_RES *res,char *consulta, int id, int conex){   
  char s[2024];
  char texto[1024];
  sprintf(texto,"<<CONV -- ID:|HORA:  |MENSAJE: \n");
  mensaje_individual(texto,conex);
  //concatena las constantes a la variable consulta 
  sprintf(consulta, "SELECT * FROM `mensajes` WHERE `conversacion_id` = '%d'", id);
 
  //hacemos consulta y verificamos que se cumple
  if((mysql_query(conn, consulta)==0)){
    //guardamos resultado en la variable resultado que es de tipo MYSQL_RES *
    res=mysql_store_result(conn);

    //leemos los datos almacenados en resultadoy lo devolvemos a la variable row que es de tipo MYSQL_ROW
    while(row=mysql_fetch_row(res)){                                           
        sprintf(s, "<<CONV -- %s | %s | %s \n",row[1], row[2], row[3]);
        mensaje_individual(s, conex);
    }
  }
  mysql_free_result(res);
  //preguntamos si se ha llegado al final de nuestra variable resultado
  if(!mysql_eof(res)){
    printf("Error de lectura %s\n", mysql_error(conn));
  }
}

/* Muestra la lista de conversaciones creadas en la BD */
void lista_conversacion(MYSQL *conn, MYSQL_ROW row, MYSQL_RES *res, char *consulta, int conex){   
  char s[2024];
  char texto[1024];
  sprintf(texto,"<<CONVER -- ID:|FECHA:      |USER1: |USER2: \n");
  mensaje_individual(texto,conex);

  //concatena las constantes a la variable consulta 
  sprintf(consulta, "SELECT * FROM `conversaciones`");

  //hacemos consulta y verificamos que se cumple
  if((mysql_query(conn, consulta)==0)){
    //guardamos resultado en la variable resultado que es de tipo MYSQL_RES *
    res=mysql_store_result(conn);
    //leemos los datos almacenados en resultadoy lo devolvemos a la variable row que es de tipo MYSQL_ROW
    while(row=mysql_fetch_row(res)){     
        sprintf(s, "<<CONVER -- %s | %s | %s | %s \n",row[0], row[1], row[2], row[3]);
        mensaje_individual(s, conex);    
    }
  }
  mysql_free_result(res);
  //preguntamos si se ha llegado al final de nuestra variable resultado
  if(!mysql_eof(res)){
    printf("Error de lectura %s\n", mysql_error(conn));
  }
}

/* FUNCION PARA LA COMUNICACIÓN */

void *comunicacion(void *arg) {

  char buff_salida[2048];
  char buff_entrada[1024];
  char bd[800];
  char consulta[1024];
  int lectura;
  int conversacion;
  
  /* ----------Fecha y hora---------- */

  time_t t;
  struct tm *tm, *tm1;
  char fecha[100], hora[50];
  t=time(NULL);
  tm=localtime(&t);
  tm1=localtime(&t);
  strftime(fecha, 100, "%d/%m/%Y", tm);
  strftime(hora, 100, "%H:%M", tm1);

  /* --------FIN Fecha y Hora-------- */
  
  cont_clietes++;
  estruc_cliente *cliente = (estruc_cliente *)arg;

  printf("<<CLIENTE ACEPTADO ");
  direccion_ip(cliente->direccion);
  printf(" ID %d\n", cliente->cid);
  cliente->saber = -1;  //Inicializamos el estado del cliente en "-1"
 
  /* Recibir información del cliente */

  while ((lectura = read(cliente->conex, buff_entrada, sizeof(buff_entrada) - 1)) > 0) {

    buff_entrada[lectura] = '\0';
    buff_salida[0] = '\0';
    salto_linea(buff_entrada);

      /* Ignorar el buffer vacío */
    if (!strlen(buff_entrada)) {
      continue;
    }
      
    /* Opciones especiales */

    if (cliente->saber > 0) { // Entra al if cuando se conecta con otro cliente, al cambiar de estado
      char *comando, *parametro, *nick;
      comando = strtok(buff_entrada, " ");          
      parametro = buff_entrada;  // parametro guarda el buff de entrada
      nick = buscar_cliente_por_id(cliente->saber);  
      
      if (parametro) {
        sprintf(buff_salida, "[%s]", cliente->nombre);
        while (parametro != NULL) {
          strcat(buff_salida, " ");
          strcat(buff_salida, parametro);
          parametro = strtok(NULL, " "); // mandar todo como una sola cadena y no dividir por espacios
        }
        strcat(buff_salida, "\r\n");
        mensaje_cliente(buff_salida, nick);
        memset(bd,'\0',sizeof(bd));
        strcpy(bd,buff_salida);

        insertar_msg(conn,cliente->conversacion, hora, bd);

        if (!strcmp(comando, "\\BYE")) { // Comando para finalizar el Chat
          for (int i = 0; i < CANT_CLIENTES; i++) {
            if (cliente->saber == clientes[i]->cid) {
              sprintf(buff_salida, "<<SALIR \n");
              mensaje_individual(buff_salida, clientes[i]->conex);
              clientes[i]->saber = -1;
              break;
            }
          }
        }
      }
    }else if (buff_entrada[0] == '\\'){
      char *comando, *parametro, *nick;
      comando = strtok(buff_entrada, " ");
      if (!strcmp(comando, "\\BYE")) {
        break;
      }else if (!strcmp(comando, "\\NAME")){

        parametro = strtok(NULL, " ");
        if (parametro) {
          char *nombre_anterior = strdup(cliente->nombre);
          strcpy(cliente->nombre, parametro);
          sprintf(buff_salida, "<<RENOMBRAR");
          free(nombre_anterior);
          mensaje_individual(buff_salida, cliente->conex);
        }else {
          mensaje_individual("<<EL NOMBRE NO PUEDE SER NULO\r\n",
           cliente->conex);
        }
      }else if (!strcmp(comando, "\\CHATEAR")){

        parametro = strtok(NULL, " ");
          if (cliente->saber < 0) {
            int saber,estado;
            if (parametro) {
              nick = parametro;
              saber = buscar_cliente_por_nombre(nick);
              estado = buscar_saber_por_nombre(nick);
              parametro = strtok(NULL, " ");
                if (estado == -1) {
                    cliente->saber = saber;   // en "saber" guarda el id del destinatario
                    for (int i = 0; i < CANT_CLIENTES; i++) {
                        if (clientes[i]) {
                            if (strcmp(clientes[i]->nombre, nick) == 0) {
                              clientes[i]->saber = cliente->cid;        // en el saber del destinatario guarda el id del origen
                              insertar_conversacion(conn,fecha,cliente->nombre,clientes[i]->nombre);

                              conversacion = buscar_id_conversacion(conn, row, res, consulta); // Guarda el ID de la última conversación
                              cliente->conversacion = conversacion; // Asignamos el ID de la conversación a los clientes que estan conectados
                              clientes[i]->conversacion = conversacion;
                            }
                          }
                      }
                  }else{
                    sprintf(buff_salida, "<<OCUPADO \n");
                    mensaje_individual(buff_salida, cliente->conex);
                  }
             }
          } 
      }else if (!strcmp(comando, "\\CONV")){

        char *id_conversacion;
        parametro = strtok(NULL, " ");
          if (parametro){
              id_conversacion = parametro;
              buscar_conversacion(conn, row, res, consulta,atoi(id_conversacion), cliente->conex);
              parametro = strtok(NULL, " ");
          }
      }else if (!strcmp(comando, "\\LIST")){

          sprintf(buff_salida, "<<CLIENTES conectados: %d\r\n", cont_clietes);
          mensaje_individual(buff_salida, cliente->conex);
          lista_clientes(cliente->conex);
      }else if (!strcmp(comando, "\\CONVER")){

          lista_conversacion(conn, row, res, consulta, cliente->conex);
        }
      }
  }

  close(cliente->conex);

  /* Eliminar cliente de la cola y el hilo */
  eliminar_cliente(cliente->cid);
  printf("<<SE DESCONECTO: %s CON ID: %d \n", cliente->nombre, cliente->cid);
  free(cliente);
  cont_clietes--;
  pthread_detach(pthread_self()); // termina el hilo que se esta usando, pthread_self() tiene el ID del hilo

  return NULL;
}