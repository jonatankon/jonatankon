#include "header.h"

int main(int argc, char *argv[]) {
  
  int sock_id = 0, sock_conex = 0;
  struct sockaddr_in dir_serv;
  struct sockaddr_in dir_cli;
  pthread_t hilo;

  /* ----------Base de datos---------- */
  char *server = "localhost";
  char *user = "root";
  char *password = "root";
  char *database = "chat";
  char consulta[1024];
  
  conn = mysql_init(NULL);
  /* Conectarse a la base de datos */
  if (!mysql_real_connect(conn, server, user, password, database, 0, NULL, 0)) {
   fprintf(stderr, "%s\n", mysql_error(conn));
  }

   /* enviar la consulta SQL */
   if (mysql_query(conn, "show tables")) {
     fprintf(stderr, "%s\n", mysql_error(conn));
     exit(1);
   }


   res = mysql_use_result(conn);
   /* imprimir los nombres de las tablas */
   printf("Tablas en la base de datos 'mysql':\n");
   while ((row = mysql_fetch_row(res)) != NULL)
    printf("%s \n", row[0]);
   /* liberar los recursos y cerrar la conexion */
   mysql_free_result(res);

/* ----------FIN Base de datos---------- */

/* CONFIGURACIÓN DEL SOCKET */

  sock_id = socket(AF_INET, SOCK_STREAM, 0);
  dir_serv.sin_family = AF_INET;
  dir_serv.sin_addr.s_addr = htonl(INADDR_ANY); // escucha todas las direcciones de la red
  dir_serv.sin_port = htons(PUERTO);

  if (bind(sock_id, (struct sockaddr *)&dir_serv, sizeof(dir_serv)) < 0) {
    perror("Socket binding failed");
    return 1;
  }
  /* ESCUCHANDO */
  if (listen(sock_id, 10) < 0) {
    perror("Socket listening failed");
    return 1;
  }

  printf("<[INICIO DEL SERVIDOR]>\n");

  /* ACEPTAR CLIENTES */
  while (1) {

      socklen_t clilen = sizeof(dir_cli);
      sock_conex = accept(sock_id, (struct sockaddr *)&dir_cli, &clilen);
        
      /* COMPRUEBA SI SE ALCANZA EL MÁXIMO DE CLIENTES */
      if ((cont_clietes + 1) == CANT_CLIENTES) {
        printf("<-----se alcanzo el máximo de clientes----->\n");
        printf("<<< cliente rechazado...");;
        printf("\n");
        close(sock_conex);
        continue;
      }

      /* CONFIGURACIONES DEL CLIENTE */
      estruc_cliente *cliente = (estruc_cliente *)malloc(sizeof(estruc_cliente));
      cliente->direccion = dir_cli;
      cliente->conex = sock_conex;
      cliente->cid = cid++;
      sprintf(cliente->nombre, "%d", cliente->cid);

      /* AGREGAR EL CLIENTE A LA COLA Y AL HILO*/
      agregar_cliente(cliente);
      pthread_create(&hilo, NULL, &comunicacion, (void *)cliente);
      sleep(1);
    }
    mysql_close(conn);
}
