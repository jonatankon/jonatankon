#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <mysql/mysql.h>

#define CANT_CLIENTES 15
#define PUERTO 5000

static unsigned int cont_clietes = 0;
static int cid = 10;

/* VARIABLES DE LA BASE DE DATOS */

  MYSQL *conn;
  MYSQL_RES *res;
  MYSQL_ROW row;

/* ESTRUCTURA DEL CLIENTE */

typedef struct {
  struct sockaddr_in direccion; /* Dirección remota del cliente */
  int conex;                    /* Descriptor de archivo de conexión */
  int cid;                      /* Identificador único del cliente */
  char nombre[32];              /* Nombre del cliente */
  int saber;					/* Identificar de estado (iddle / busy) */
  int conversacion;				/* Identificador de ID de conversación */
} estruc_cliente;

estruc_cliente *clientes[CANT_CLIENTES];

/* FUNCIONES GENERALES */

void agregar_cliente(estruc_cliente *cl);

int buscar_cliente_por_nombre(char *nom);

char *buscar_cliente_por_id(int cid);

int buscar_saber_por_nombre(char *nom);

void eliminar_cliente(int cid);

void enviar_mensaje(char *s, int cid);

void mensaje_individual(const char *s, int conex);

void mensaje_general(char *s);

void mensaje_cliente(char *s, char *nombre);

void lista_clientes(int conex);

void salto_linea(char *s);

void direccion_ip(struct sockaddr_in direccion);

/* FUNCIONES PARA LA BASE DE DATOS */

void insertar_msg(MYSQL *conn, int conversacion, char *hora, char *msg);

void insertar_conversacion(MYSQL *conn, char *fecha, char *user1, char *user2);

int buscar_id_conversacion(MYSQL *conn, MYSQL_ROW row, MYSQL_RES *res, char *consulta);

void buscar_conversacion(MYSQL *conn, MYSQL_ROW row, MYSQL_RES *res,char *consulta, int id, int conex);

void lista_conversacion(MYSQL *conn, MYSQL_ROW row, MYSQL_RES *res, char *consulta, int conex);

/* FUNCION PARA LA COMUNICACIÓN */

void *comunicacion(void *arg);
