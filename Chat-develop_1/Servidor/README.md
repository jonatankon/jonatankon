# // Instrucciones para la instalación y conexión del servidor //

1. Ingresar a la carpeta "/src".
2. Abrir la terminal en dicha carpeta, ejectuar comando "make carpeta" para crear las carpetas "\bin" y "obj" (sólo la primera vez).
3. Luego ejecutar comando "make" para compilar y generar el ejecutable.
3. Volver una carpeta atrás e ingresar en la carpeta "/bin".
4. Ejecutar en la terminal "$ ./servidor".

