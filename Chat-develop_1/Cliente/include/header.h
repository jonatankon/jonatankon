#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PUERTO 5000

typedef struct {
  int sock;
  int chat;
  char nickname[50];
} cliente_data;

/* FUNCION PARA LEER EL BUFFER DE ENTRADA */

void imprimir_bienvenida();
void *leer_bufer(void *arg);

