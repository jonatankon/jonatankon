# // Instrucciones para la instalación y conexión del cliente con el servidor //

1. Ingresar a la carpeta "/src".
2. Abrir la terminal en dicha carpeta, ejectuar comando "make carpeta" para crear las carpetas "\bin" y "obj" (sólo la primera vez).
3. Luego ejecutar comando "make" para compilar y generar el ejecutable.
4. Volver una carpeta atrás e ingresar en la carpeta "/bin".
5. Ejecutar en la terminal "$ ./cliente" (anteriormente iniciado el servidor).

# // Comandos disponibles a utilizar //

1. $ \list -> imprime por consola los clientes conectados al servidor.
2. $ \chatear <nickname> -> inicia una conversación con un cliente.
3. $ \conver -> imprime por consola las conversaciones cargadas en la BD.
4. $ \conv <id_conversacion> -> imprime los mensajes de una convesación cargada en la BD.
5. $ \bye -> Para la desconexión de la aplicación.
