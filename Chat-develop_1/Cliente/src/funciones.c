#include "header.h"

/* FUNCION GENERAL */

void imprimir_bienvenida() {
    printf("==============================================================\n");
    printf(" ######\n #     # # ###### #    # #    # ###### #    # # #####   #### \n #     # # #      ##   # #    # #      ##   # # #    # #    # \n ######  # #####  # #  # #    # #####  # #  # # #    # #    # \n #     # # #      #  # # #    # #      #  # # # #    # #    # \n #     # # #      #   ##  #  #  #      #   ## # #    # #    # \n ######  # ###### #    #   ##   ###### #    # # #####   ####  \n");

    printf("==============================================================\n");
    printf("==============================================================\n");
}

void *leer_bufer(void *arg) {

  cliente_data *args = (cliente_data *)arg;
  time_t t;
  struct tm *tm;
  char hora[100];
  char *tmp;
  int longmax = 512;
  char pbuffer[longmax];
  char *linea = NULL;
  char emisor[longmax];

  while (recv(args->sock, pbuffer, longmax, 0) > 0) {

    linea = strtok(pbuffer, "\r\n");
    while (linea != NULL) {

      if (strstr(linea, "<<CLIENTES ") != NULL) {//lista de clientes
        printf(">> clientes %s \n", linea + 10);
      }

      if (strstr(linea, "<<CONVER ") != NULL) { //lista de conversaciones
        printf(". \n %s", linea + 10);
      }

      if (strstr(linea, "<<CONV ") != NULL) { //lista de mensajes
        printf(". \n %s", linea + 10);
      }

      if (strstr(linea, "<<OCUPADO ") != NULL) { // mensaje indicando que esta ocupado
        printf(">>El cliente esta ocupado !! %s", linea + 10);
        args->chat = 0;
      }

      if (strstr(linea, "<<SALIR ") != NULL) { // mensaje al desconectarse un cliente con el que esta chateando
        args->chat = 0;
        printf("Finalizo la conversacion, escriba un comando: %s \n", linea + 10);
      }

      if (strstr(linea, "<<CLIENTE ") != NULL) { 
        printf("\t- %s", linea + (strchr(linea, '|') - linea + 2));
      }

      if (strstr(linea, "<<RENOMBRAR") != NULL) { // indica los comandos disponibles
        printf("Tu nickname es %s \r\n Comandos: \n \\list --> lista de clientes \n \\chatear <nickname> --> chatear \n \\conver --> conversaciones \n \\conv <id> --> mensajes \n \\bye --> salir \n", args->nickname);
      }
      if (linea[0] == '[') { // indica el formato de como se recibe el mensaje
        t = time(NULL);
        tm = localtime(&t);
        strftime(hora, 100, "(%H:%M) -> ", tm);
        strncpy(emisor, linea + 1, strchr(linea, ']') - (linea + 1));
        emisor[strchr(linea, ']') - (linea + 1) + 1] = '\0';
        printf("%s %s %s\r\n> ", emisor, hora,
         linea + ((strchr(linea, ']') - linea) + 2));

        args->chat = 1;
      }

      linea = strtok(NULL, "\r\n");
    }
    memset(pbuffer, '\0', longmax);
    sleep(0.1);
  }
  return NULL;
}