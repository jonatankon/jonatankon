#   MINT-CHAT

# // Instrucciones para descargar el repositorio //

1.	Instalar Git en su máquina, URL: https://git-scm.com/
2.	Situarse donde desea clonar y ejecute por terminal el siguiente comando: 
git clone https://jonatankon@bitbucket.org/jonatankon/jonatankon.git
3.	Una vez clonado podrá acceder a las carpetas Cliente y Servidor con sus respectivos instructivos (previamente instalar phpmyadmin).

# // Instalación phpmyadmin necesario para la Base de Datos //

1.	Instalar la ultima versión de phpmyadmin, URL:https://www.phpmyadmin.net/.
2.	Ingresar a phpmyadmin, seleccionar la opción "importar" y seleccionamos el archivo de la BD que se encuentra en "/Servidor/bd/chat.sql".
