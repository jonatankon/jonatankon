#include "header.h"

int main() {

  const char *nombre_serv = "localhost";
  char mensaje[1000], comando[1000], enviar[1000];
  int exit = 0, sock_id;
  time_t t;
  struct tm *tm;
  char hora[100], *tmp, buf[22];
  cliente_data cli;

  /* Es donde configuras la estructura del socket(asocias el socket a un puerto).*/

  struct sockaddr_in dir_serv;
  memset(&dir_serv, 0, sizeof(dir_serv)); // rellena el bloque de memoria con un valor particular(0).
  dir_serv.sin_family = AF_INET; //sin_family es el protocolo de comunicacion del socket --AF_INET para protocolos de internet version 4, ipv4.
  inet_pton(AF_INET, nombre_serv, &dir_serv.sin_addr); //acepta una cadena decimal con puntos estandar ipv4.
  dir_serv.sin_port = htons(PUERTO); //puerto al que se asocia el socket.

  //creacion del socket, y usamos SOCK_STREAM es una conexion bireccional confiable de flujo de datos ordenados
  if ((cli.sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) { 

    printf("could not create socket\n");
    return 1;
  }

  //connect solicita la conexion con el servidor. 0 --> establece la conexion si no larga error
  if (connect(cli.sock, (struct sockaddr *)&dir_serv, sizeof(dir_serv)) < 0) {
    printf("could not connect to server\n");
    return 1;
  }

  imprimir_bienvenida();
  printf("\n\t\t\t>>> MINT-CHAT <<<<\n\n");

  pthread_t hilo;
  //nombre del hilo, valores predeterminados, cuando arranca el hilo ejecuta la funcion, argumento de la funcion
  pthread_create(&hilo, NULL, leer_bufer, (void*)&cli); 
  sleep(0.1);

  while (1) {
    if (strlen(cli.nickname) == 0) {

      printf("Ingrese un nickname para chatear: ");
      fgets(mensaje, 1000, stdin);
      strcpy(enviar, "\\NAME ");
      strcat(enviar, mensaje);
      strcpy(cli.nickname, mensaje);

    } else {

      t = time(NULL);
      tm = localtime(&t);
      strftime(hora, 100, "(%H:%M) -> ", tm);
      printf("%s %s", cli.nickname, hora);
      fgets(comando, 1000, stdin);

      if (strchr(comando, '\n') != NULL) {
        comando[strchr(comando, '\n') - comando] = '\0';
      }

      if (strchr(comando, '\r') != NULL) {
        comando[strchr(comando, '\r') - comando] = '\0';
      }

      if (strstr(comando, "\\bye") != NULL) {
        strcpy(enviar, "\\BYE");
        exit = 1;

        // strstr --> verifica el principio de la cadena y entra
      }else if (strstr(comando, "\\list") != NULL) {
        strcpy(enviar, "\\LIST");
      
      }else if (strstr(comando, "\\conver") != NULL) {
        strcpy(enviar, "\\CONVER");
      
      }else if (strstr(comando, "\\chatear") != NULL) {
        strcpy(enviar, "\\CHATEAR ");
        strcat(enviar, comando + 9);
        cli.chat = 1;

      }else if (strstr(comando, "\\conv") != NULL) {
        strcpy(enviar, "\\CONV ");
        strcat(enviar, comando + 6);

      } else {
        if (cli.chat == 0) {
          printf(">> comando desconocido: %s\r\n", comando);
          continue;
        } else {
          strcpy(enviar, comando);
        }
      }
    }

    send(cli.sock, enviar, strlen(enviar), 0);
    sleep(1);
    printf("\n");

    if (exit == 1) {
      printf("Hasta pronto %s\r\n", cli.nickname);
      break;
    }
  }
  close(cli.sock);
  return 0;
}
